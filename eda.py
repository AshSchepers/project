
from state import State
from edge import Edge
from letter import Letter
from automata import Automata

multi_paths = False;
ranges = {}
ranges['.'] = "!-~"
ranges['\w'] = "A-Z0-9a-z_-_"
ranges['\d'] = "0-9"
ranges['\s'] = " "
ranges['\D'] = "!-/:-~"
ranges['\W'] = "!-/:-@[_^`-`{-~"

def determine_eda(fsm: Automata):
    global multi_paths
    global eda_state
    global eda_path1
    global eda_path2

    eda_state = {}
    eda_path1 = []
    eda_path2 = []

    for state in fsm.states:
        if not multi_paths:
            dfs(state, state.next_states, [], [])
            multi_paths = False

    return eda_state


#marked should be list of transitions
def dfs(state: State, marked: dict, paths: list, path: list):
    global multi_paths
    global eda_state
    global eda_path1
    global eda_path2
    quantified = False
    quantified_path = None
    quantified_nums = None
    quantify_index = 0

    for k, l in marked.items():
        i = 0
        for v in l:
            if not v[2]:
                #if str(v[0]) == str(state):
                if v[0] == state:
                    if "{" in k and "}" in k and "[" not in k:
                        if paths == []:
                            continue
                        beg = int(k[1])
                        end = beg

                        try:
                            end = int(k[-1])
                        except:
                            try:
                                end = int(k[-2])
                            except:
                                pass
                        if quantified:
                            if beg > quantified_nums[0]:
                                quantified_path = len(paths) - 1
                                quantified_nums = (beg, end)
                                quantify_index = len(paths[-1]) - 1
                        else:
                            quantified = True
                            quantified_path = len(paths) - 1
                            quantified_nums = (beg, end)
                            quantify_index = len(paths[-1]) - 1
                    else:
                        multi_paths |= check_multi(paths, path + [k])
                    if multi_paths:
                        eda_path1 = path + [k]
                        eda_state[state] = [eda_path1, eda_path2]
                        #return
                    paths.append(path + [k])
                    marked[k][i] = [v[0], v[1], True]
                else:
                    marked[k][i] = [v[0], v[1], True]
                    if "{" in k and "}" in k and "[" not in k:
                        if paths == []:
                            continue
                        beg = int(k[1])
                        end = beg

                        try:
                            end = int(k[-1])
                        except:
                            try:
                                end = int(k[-2])
                            except:
                                pass
                        if quantified:
                            if beg > quantified_nums[0]:
                                quantified_path = len(paths) - 1
                                quantified_nums = (beg, end)
                                quantify_index = len(paths[-1]) - 1
                        else:
                            quantified = True
                            quantified_path = len(paths) - 1
                            quantified_nums = (beg, end)
                            quantify_index = len(paths[-1]) - 1

                    else:
                        dfs(state, v[0].next_states, paths, path + [k])
                        marked[k][i] = [v[0], v[1], False]
            i += 1
    if quantified:
        changed = False
        p = paths[quantified_path]
        pre_p = p[:quantify_index]
        pump_p = [p[quantify_index]]
        suff_p = []
        vals = []
        if quantify_index < len(p) - 1:
            suff_p = p[quantify_index+1:]

        paths = paths[:quantified_path]
        if quantified_path < len(paths) - 1:
            paths += paths[quantified_path+1:]
        for i in range(quantified_nums[0] - 1):
            pump_p += [pump_p[0]]
        c = [pre_p + pump_p + suff_p]
        paths += c
        i = (len(paths) - 1)
        vals.append(i)

        if quantified_nums[0] != quantified_nums[1]:
            for i in range(quantified_nums[1] - quantified_nums[0]):
                pump_p += pump_p[0]
                paths += [pre_p + pump_p + suff_p]
                vals.append(len(paths) - 1)

        for i in range(len(paths)):
            for j in range(len(paths)):
                if i != j and (i not in vals and j not in vals):
                    multi_paths |= check_multi(paths[i], paths[j])
                    if multi_paths:
                        changed = True
                        eda_state[state] = [paths[i], paths[j]]
        if not changed:
            multi_paths = False


def check_multi(paths: list, path: list):
    global eda_path2
    multi = False
    if paths == []:
        return False

    for p in paths:
        #for i in range(min(len(p), len(path))):
        #    if not check_range(p[i], path[i]):
        #        return False
        if len(p) != len(path):
            if (len(p) % len(path) != 0) and (len(path) % len(p) != 0):
                continue
            else:
                change = True
                if len(p) < len(path):
                    k = 0
                    for i in range(int(len(path)/len(p))):
                        for j in range(len(p)):
                            if not check_range(p[j], path[k]):
                                change = False
                            k += 1
                    if change:
                        multi = True
                        eda_path2 = p
                else:
                    k = 0
                    change = True
                    for i in range(int(len(p)/len(path))):
                        for j in range(len(path)):
                            if not check_range(path[j], p[k]):
                                change = False
                            k += 1
                    if change:
                        multi = True
                        eda_path2 = p
        else:
            change = True
            for i in range(len(p)):
                if not check_range(path[i], p[i]):
                    change = False
            if change:
                multi = True
                eda_path2 = p
    return multi

def check_range(r1: str, r2: str):
    if r1 in ranges: 
        r1s = ranges[r1]
    else: 
        r1s = r1
    if r2 in ranges: 
        r2s = ranges[r2]
    else: 
        r2s = r2

    if r1s.find('-') == -1 and r2s.find('-') == -1:
        return (r1s in r2s and len(r2s) % len(r1s) == 0) or (r2s in r1s and len(r1s) % len(r2s) == 0)

    start_r1 = 0
    start_r2 = 0
    ind_r2 = r2s.find('-', start_r2) 

    while ind_r2 != -1:
        ind_r1 = r1s.find('-', start_r1) 
        while ind_r1 != -1:
            if r1s[start_r1] <= r2s[start_r2] and r2s[ind_r2+1] <= r1s[ind_r1+1]:
                return True
            if r2s[start_r1] <= r1s[start_r2] and r1s[ind_r2+1] <= r2s[ind_r1+1]:
                return True
            start_r1 = ind_r1 + 1
            ind_r1 = r1s.find('-', start_r1) 
        start_r2 = ind_r2 + 1
        ind_r2 = r2s.find('-', start_r2) 
 
    return False


