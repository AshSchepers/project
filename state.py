
from letter import Letter 

class State:
    """Represents a state in a fsm. """

    """
    name is the name of the state.
    accepting states whether or not it is an accepting state.
    next_states is a dictionary with character transitions as keys and states as
    values.
    """
    def __init__(self, expr_numb: int, name: int, accepting=False, split=False, normal=True,
            join=False):
        self.__expr_numb = expr_numb
        self.__name = name
        self.__accepting = accepting
        self.__next_states = {}
        self.__split = split
        self.__normal = normal
        self.__join = join


    @property
    def expr_numb(self):
        return self.__expr_numb

    @property
    def name(self):
        return self.__name

    @property
    def accepting(self):
        return self.__accepting

    @property
    def join(self):
        return self.__join

    @property
    def split(self):
        return self.__split

    @property
    def normal(self):
        return self.__normal

    @property
    def next_states(self):
        return self.__next_states

    @join.setter
    def join(self, join: bool):
        self.__join = join

    @split.setter
    def split(self, split: bool):
        self.__split = split
        self.__normal =  not split

    @normal.setter
    def normal(self, normal: bool):
        self.__normal = normal
        self.__split =  not normal

    def add_transition(self, key: Letter, state: 'State'):
        if key.eps:
            c = ''
        elif key.character:
            c = str(key)
        else:
            c = key.char

        if c in self.__next_states:
            if state not in self.__next_states[c]:
                self.__next_states[c].append([state, key.priority, False])
                self.split = True
            else:
                #TODO
                pass
        else:
            self.__next_states[c] = [[state, key.priority, False]]
            if len(self.__next_states) > 1:
                self.split = True


    def __str__(self):
        return str(self.__name)
