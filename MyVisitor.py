
from PCREVisitor import PCREVisitor
from PCREParser import PCREParser
from letter import Letter
from automata import Automata
from state import State
from edge import Edge
import sys

class MyVisitor(PCREVisitor):

    def __init__(self):
        self.expr_number = 0
        self.is_group = False

    def createAutomata(self, final=False) -> Automata:
        a = Automata()
        s = State(self.expr_number, self.name, final)
        if final:
            a.final_state = s
        self.name += 1
        a.initial_state = s
        return a


    # Create p1 -> p2 -> s
    def linkAutomata(self, p1: Automata, p2: Automata):
        p1.add_transition(p1.final_state, p2.initial_state, 
                Letter(self.expr_number, eps=True))
        s = State(self.expr_number, self.name, True)
        self.name += 1
        p1.add_transition(p2.final_state, s, Letter(self.expr_number, eps=True))
        p1.final_state = s
        p2.final_state = s

    # Create p1 -> p2 
    def linkCCAutomata(self, p1: Automata, p2: Automata):
        p1.add_transition(p1.initial_state, p2.initial_state, 
                Letter(self.expr_number, eps=True))
        s = State(self.expr_number, self.name, True)
        self.name += 1
        p1.add_transition(p2.final_state, s, Letter(self.expr_number, eps=True))
        p1.final_state = s
        p2.final_state = s


    def createLinkAutomata(self, func, final=False) -> Automata:
        a = self.createAutomata(final)
        p = self.visit(func)
        #a.show()
        #print(a.final_state.name)
        self.linkAutomata(a, p)
        a.states = (a.states).union(p.states)
        return a


    # Create a -text-> s
    def createBaseAutomata(self, text):
        a = self.createAutomata()
        s = State(self.expr_number, self.name)
        a.add_transition(a.initial_state, s, Letter(self.expr_number, text))
        a.final_state = s
        self.name += 1
        return a


    # Create a -text-> s
    def createLinkedAutomata(self, text):
        a = self.createAutomata(final=True)
        for t in text:
            s = State(self.expr_number, self.name)
            a.add_transition(a.final_state, s, Letter(self.expr_number, t))
            a.final_state = s
            self.name += 1
        a.final_state = s
        return a


    # Visit a parse tree produced by PCREParser#parse.
    def visitParse(self, ctx:PCREParser.ParseContext):
        self.states = set()
        self.accept_states = set()
        self.name = 0
        self.fsm = self.createAutomata(final=True)
        a = self.visit(ctx.alternation())
        self.linkAutomata(self.fsm, a)
        self.fsm.states = (self.fsm.states).union(a.states)
        return self.fsm


    # Visit a parse tree produced by PCREParser#alternation.
    def visitAlternation(self, ctx:PCREParser.AlternationContext):
        a = self.createAutomata(final=True)
        #print(len(ctx.expr()))
        s = State(self.expr_number, self.name, True)
        self.name += 1
        priority = 1
        for expr in ctx.expr():
            partial_automaton = self.buildPartialAutomaton(expr)
            #partial_automaton.show()
            if not self.is_group:
                self.expr_number += 1

            a.add_transition(a.initial_state, partial_automaton.initial_state, Letter(self.expr_number, self.expr_number, eps=True, priority=priority))
            a.add_transition(partial_automaton.final_state, s, Letter(self.expr_number, eps=True))
            a.states = (a.states).union(partial_automaton.states)
            a.add_accept_state(s)
            priority += 1
        a.final_state = s
        return a


    def buildPartialAutomaton(self, expr:PCREParser.ExprContext):
        a = self.createAutomata(final=True)
        s = None

        #return self.createLinkAutomata(expr.atom())
        for element in expr.element():
            if element.quantifier() is None and element.atom().literal() is not None:
                if s is None:
                    s = element.getText()
                else:
                    s += element.getText()
            else:
                #print("quantifier")
                if s is not None:
                    p = self.createLinkedAutomata(s)
                    self.linkAutomata(a, p)
                    a.states = (a.states).union(p.states)
                    s = None

                self.__cur_aut = a
                self.__cur_state = a.final_state

                #print('quantifying')
                tmp = element.atom().literal()
                if tmp is not None and tmp.shared_literal() is not None:
                    p = self.createLinkedAutomata(tmp.getText())
                else:
                    is_inner_group = self.is_group

                    if element.atom().capture() is not None or element.atom().non_capture() is not None:
                        self.is_group = True
                        self.expr_number += 1

                    p = self.visit(element.atom())

                    self.is_group = is_inner_group
                    #p.show()
                    if element.atom().capture() is not None or element.atom().non_capture() is not None:
                        self.__cur_state = p.initial_state

                self.linkAutomata(a, p)
                a.states = (a.states).union(p.states)
                #a.show()
                if element.quantifier() is not None:
                    if element.atom().capture() is not None or element.atom().non_capture() is not None:
                        self.__cur_aut = a

                    A = self.visit(element.quantifier())
                    if A is not None:
                        self.linkAutomata(a, A)

                    #a.show()

                #print(element.quantifier().starGreedy())

        if s is not None:
            #print()
            #print(s)
            p = self.createLinkedAutomata(s)
            self.linkAutomata(a, p)
            a.states = (a.states).union(p.states)

        #a.show()
        return a


    # Visit a parse tree produced by PCREParser#expr.
    def visitExpr(self, ctx:PCREParser.ExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#element.
    def visitElement(self, ctx:PCREParser.ElementContext):
        return self.visitChildren(ctx)

    # Visit a parse tree produced by PCREParser#oneGreedy.
    def visitOneGreedy(self, ctx:PCREParser.OneGreedyContext):
        self.__cur_aut.add_transition(self.__cur_state, self.__cur_aut.final_state, Letter(self.expr_number, eps=True, priority=1))

        for state in self.__cur_state.next_states['']:
            if not state[0] == self.__cur_aut.final_state:
                state[1] += 1


    # Visit a parse tree produced by PCREParser#onePossessive.
    def visitOnePossessive(self, ctx:PCREParser.OnePossessiveContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#oneLazy.
    def visitOneLazy(self, ctx:PCREParser.OneLazyContext):
        self.__cur_aut.add_transition(self.__cur_state, self.__cur_aut.final_state, Letter(self.expr_number, eps=True, priority=2))
        return None


    # Visit a parse tree produced by PCREParser#plusGreedy.
    def visitPlusGreedy(self, ctx:PCREParser.PlusGreedyContext):
        self.__cur_aut.add_transition(self.__cur_aut.final_state, self.__cur_state, Letter(self.expr_number, eps=True, priority=1))

        s = State(self.expr_number, self.name, True)
        self.name += 1
        self.__cur_aut.add_transition(self.__cur_aut.final_state, s, Letter(self.expr_number, eps=True, priority=2))
        self.__cur_aut.final_state = s
        return None


    # Visit a parse tree produced by PCREParser#plusPossessive.
    def visitPlusPossessive(self, ctx:PCREParser.PlusPossessiveContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#plusLazy.
    def visitPlusLazy(self, ctx:PCREParser.PlusLazyContext):
        self.__cur_aut.add_transition(self.__cur_aut.final_state, self.__cur_state, Letter(self.expr_number, eps=True, priority=1))

        s = State(self.expr_number, self.name, True)
        self.name += 1
        self.__cur_aut.add_transition(self.__cur_aut.final_state, s, Letter(self.expr_number, eps=True, priority=1))

        if '' in self.__cur_aut.final_state.next_states:
            for state in self.__cur_aut.final_state.next_states['']:
                if not state[0] == s:
                    state[1] += 1

        self.__cur_aut.final_state = s
        return None

    # Visit a parse tree produced by PCREParser#starGreedy.
    def visitStarGreedy(self, ctx:PCREParser.StarGreedyContext):
        self.__cur_aut.add_transition(self.__cur_aut.final_state, self.__cur_state, Letter(self.expr_number, eps=True, priority=1))
        #self.__cur_aut.show()

        s = State(self.expr_number, self.name, True)
        self.name += 1
        self.__cur_aut.add_transition(self.__cur_state, s, Letter(self.expr_number, eps=True, priority=2))
        self.__cur_aut.final_state = s
        return None


    # Visit a parse tree produced by PCREParser#starPossessive.
    def visitStarPossessive(self, ctx:PCREParser.StarPossessiveContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#starLazy.
    def visitStarLazy(self, ctx:PCREParser.StarLazyContext):
        self.__cur_aut.add_transition(self.__cur_aut.final_state, self.__cur_state, Letter(self.expr_number, eps=True, priority=1))

        s = State(self.expr_number, self.name, True)
        self.name += 1
        self.__cur_aut.add_transition(self.__cur_state, s, Letter(self.expr_number, eps=True, priority=1))
        self.__cur_aut.final_state = s


        for state in self.__cur_state.next_states['']:
            if not state[0] == self.__cur_aut.final_state:
                state[1] += 1
        return None

    # Visit a parse tree produced by PCREParser#nElements.
    def visitNElements(self, ctx:PCREParser.NElementsContext):
        return self.createBaseAutomata(ctx.getText())
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#nElementsGreedy.
    def visitNElementsGreedy(self, ctx:PCREParser.NElementsGreedyContext):
        return self.createBaseAutomata(ctx.getText())
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#nElementsPossessive.
    def visitNElementsPossessive(self, ctx:PCREParser.NElementsPossessiveContext):
        return self.createBaseAutomata(ctx.getText())
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#nElementsLazy.
    def visitNElementsLazy(self, ctx:PCREParser.NElementsLazyContext):
        return self.createBaseAutomata(ctx.getText())
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#nmGreedy.
    def visitNmGreedy(self, ctx:PCREParser.NmGreedyContext):
        return self.createBaseAutomata(ctx.getText())
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#nmPossessive.
    def visitNmPossessive(self, ctx:PCREParser.NmPossessiveContext):
        return self.createBaseAutomata(ctx.getText())
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#nmLazy.
    def visitNmLazy(self, ctx:PCREParser.NmLazyContext):
        return self.createBaseAutomata(ctx.getText())
        return self.visitChildren(ctx)




#    # Visit a parse tree produced by PCREParser#quantifier.
#    def visitQuantifier(self, ctx:PCREParser.QuantifierContext):
#        q = ctx.quantifier().getText()
#        return self.visitChildren(ctx)
#
#
#    # Visit a parse tree produced by PCREParser#quantifier_type.
#    def visitQuantifier_type(self, ctx:PCREParser.Quantifier_typeContext):
#        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#charHyphenOpCC.
    def visitCharHyphenOpCC(self, ctx:PCREParser.CharHyphenOpCCContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#charOpCC.
    def visitCharOpCC(self, ctx:PCREParser.CharOpCCContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#opCC.
    def visitOpCC(self, ctx:PCREParser.OpCCContext):
        return self.visitChildren(ctx)


    #TODO take into account hyphens
    # Visit a parse tree produced by PCREParser#charHyphenCC.
    def visitCharHyphenCC(self, ctx:PCREParser.CharHyphenCCContext):
        aut = self.createAutomata(final=True)
        char_range = []
        char_range.append((']', ctx.cc_atom()[0].getText()))

        for atom in ctx.cc_atom()[1:]:
            if atom.Hyphen():
                l1 = atom.cc_literal()[0].getText()
                l2 = atom.cc_literal()[1].getText()

                char_range.append((l1, l2))
            else:
                char_range.append(tuple(atom.getText()))

        s = State(self.expr_number, self.name)
        self.name += 1
        aut.add_transition(aut.final_state, s, Letter(self.expr_number, character=True, char_range=char_range))
        aut.final_state = s
        return aut


    #TODO take into account hyphens
    # Visit a parse tree produced by PCREParser#charCC.
    def visitCharCC(self, ctx:PCREParser.CharCCContext):
        aut = self.createAutomata(final=True)
        char_range = []
        char_range.append(tuple(']'))

        for atom in ctx.cc_atom():
            if atom.Hyphen():
                l1 = atom.cc_literal()[0].getText()
                l2 = atom.cc_literal()[1].getText()

                char_range.append((l1, l2))
            else:
                char_range.append(tuple(atom.getText()))

        s = State(self.expr_number, self.name)
        self.name += 1
        aut.add_transition(aut.final_state, s, Letter(self.expr_number, character=True, char_range=char_range))
        aut.final_state = s
        #aut.show()
        return aut


    #TODO take into account hyphens
    # Visit a parse tree produced by PCREParser#normalCC.
    def visitNormalCC(self, ctx:PCREParser.NormalCCContext):
        aut = self.createAutomata(final=True)
        char_range = []

        for atom in ctx.cc_atom():
            if atom.Hyphen():
                l1 = atom.cc_literal()[0].getText()
                l2 = atom.cc_literal()[1].getText()

                char_range.append((l1, l2))
            else:
                #TODO
                if len(atom.cc_literal()) > 0 and atom.cc_literal()[0].shared_literal() is not None:
                    char_range.append(tuple(atom.getText()))
                elif len(atom.cc_literal()) == 0:
                    char_range.append(tuple(atom.getText()))
                else:
                    char_range.append(tuple('\\' + atom.getText()))

        s = State(self.expr_number, self.name)
        self.name += 1
        aut.add_transition(aut.final_state, s, Letter(self.expr_number, character=True, char_range=char_range))
        aut.final_state = s
        #aut.show()
        return aut


    # Visit a parse tree produced by PCREParser#backreference.
    def visitBackreference(self, ctx:PCREParser.BackreferenceContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#backreference_or_octal.
    def visitBackreference_or_octal(self, ctx:PCREParser.Backreference_or_octalContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#capture.
    def visitCapture(self, ctx:PCREParser.CaptureContext):
        a = self.createAutomata(final=True)

        if ctx.name() is None:
            p = self.visit(ctx.alternation())
            #p.show()
            #print(a.final_state.name)
            #print(p.final_state.name)
            self.linkAutomata(a, p)
            a.states = (a.states).union(p.states)

        return a


    # Visit a parse tree produced by PCREParser#non_capture.
    def visitNon_capture(self, ctx:PCREParser.Non_captureContext):
        return self.visitChildren(ctx)


    #TODO not supported
    # Visit a parse tree produced by PCREParser#comment.
    def visitComment(self, ctx:PCREParser.CommentContext):
        return self.visitChildren(ctx)


    #TODO not supported
    # Visit a parse tree produced by PCREParser#option.
    def visitOption(self, ctx:PCREParser.OptionContext):
        return self.visitChildren(ctx)


    #TODO not supported
    # Visit a parse tree produced by PCREParser#option_flags.
    def visitOption_flags(self, ctx:PCREParser.Option_flagsContext):
        return self.visitChildren(ctx)


    #TODO not supported
    # Visit a parse tree produced by PCREParser#option_flag.
    def visitOption_flag(self, ctx:PCREParser.Option_flagContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#look_around.
    def visitLook_around(self, ctx:PCREParser.Look_aroundContext):
        return self.visitChildren(ctx)


    #TODO not supported
    # Visit a parse tree produced by PCREParser#subroutine_reference.
    def visitSubroutine_reference(self, ctx:PCREParser.Subroutine_referenceContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#conditional.
    def visitConditional(self, ctx:PCREParser.ConditionalContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#backtrack_control.
    def visitBacktrack_control(self, ctx:PCREParser.Backtrack_controlContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#newline_convention.
    def visitNewline_convention(self, ctx:PCREParser.Newline_conventionContext):
        return self.visitChildren(ctx)


    #TODO not supported
    # Visit a parse tree produced by PCREParser#callout.
    def visitCallout(self, ctx:PCREParser.CalloutContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#atom.
    def visitAtom(self, ctx:PCREParser.AtomContext):
        if ctx.literal() is not None:
            a = self.createLinkAutomata(ctx.literal(), final=True)
        elif ctx.backreference():
            raise Exception("Backreferences not supported.")
        elif ctx.look_around():
            raise Exception("Look arounds not supported.")
        elif ctx.subroutine_reference():
            raise Exception("Subroutine references not supported.")
        elif ctx.capture() or ctx.non_capture() or ctx.conditional() or ctx.character_class():
            a = self.visitChildren(ctx)
        else:
            return self.createBaseAutomata(ctx.getText())

        #a.show()
        return a


    # Visit a parse tree produced by PCREParser#cc_atom.
    def visitCc_atom(self, ctx:PCREParser.Cc_atomContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#shared_atom.
    def visitShared_atom(self, ctx:PCREParser.Shared_atomContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#literal.
    def visitLiteral(self, ctx:PCREParser.LiteralContext):
        a = self.createLinkAutomata(ctx.shared_literal(), final=True)
        #a.show()
        return a


    # Visit a parse tree produced by PCREParser#cc_literal.
    def visitCc_literal(self, ctx:PCREParser.Cc_literalContext):
        if ctx.shared_literal():
            return self.visitChildren(ctx)
        else:
            return self.createBaseAutomata('\\' + ctx.getText())


    # Visit a parse tree produced by PCREParser#shared_literal.
    def visitShared_literal(self, ctx:PCREParser.Shared_literalContext):
        if ctx.letter() is not None:
            a = self.createLinkAutomata(ctx.letter(), final=True)
            return a
        elif ctx.digit() is not None:
            return self.createLinkAutomata(ctx.digit(), final=True)
        else:
            return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#number.
    def visitNumber(self, ctx:PCREParser.NumberContext):
        return self.createBaseAutomata(ctx.getText())


    # Visit a parse tree produced by PCREParser#octal_char.
    def visitOctal_char(self, ctx:PCREParser.Octal_charContext):
        return self.createBaseAutomata(ctx.getText())


    # Visit a parse tree produced by PCREParser#octal_digit.
    def visitOctal_digit(self, ctx:PCREParser.Octal_digitContext):
        return self.createBaseAutomata(ctx.getText())


    # Visit a parse tree produced by PCREParser#digits.
    def visitDigits(self, ctx:PCREParser.DigitsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#digit.
    def visitDigit(self, ctx:PCREParser.DigitContext):
        # Check camel case
        return self.createBaseAutomata(ctx.getText())


    # Visit a parse tree produced by PCREParser#name.
    def visitName(self, ctx:PCREParser.NameContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#alpha_nums.
    def visitAlpha_nums(self, ctx:PCREParser.Alpha_numsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#non_close_parens.
    def visitNon_close_parens(self, ctx:PCREParser.Non_close_parensContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#non_close_paren.
    def visitNon_close_paren(self, ctx:PCREParser.Non_close_parenContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#letter.
    def visitLetter(self, ctx:PCREParser.LetterContext):
        # Check camel case
        a = self.createBaseAutomata(ctx.getText())
        #a.show()
        return a



del PCREParser
