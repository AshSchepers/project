
import sys
from antlr4 import *
from antlr4.InputStream import InputStream
from PCRELexer import PCRELexer
from PCREParser import PCREParser
from MyVisitor import MyVisitor
from minimize import *
from eda import determine_eda
from exploit import exploit_string

def recurse_group(input_stream: str, pos: int):
    inside_groups = 0
    quantifiers = set()
    quantifiers.add("*")
    quantifiers.add("?")
    quantifiers.add("+")
    done = False
    subexpression = "("
    while not done:
        pos += 1
        if input_stream[pos] == "(" and input_stream[pos-1] != "\\":
            inside_groups += 1
        elif input_stream[pos] == ")" and input_stream[pos-1] != "\\":
            if inside_groups != 0:
                inside_groups -= 1
            else: 
                done = True
        subexpression += input_stream[pos]
    if pos < len(input_stream) and input_stream[pos+1] in quantifiers:
        subexpression += input_stream[pos+1]
    return subexpression


def find_subexpression(input_stream: str, expr_numb: int):
    i = 0
    j = 0
    while i < expr_numb:
        if input_stream[j] == "(":
            if j > 0 and input_stream[j-1] == "\\":
                j += 1
            else:
                i += 1
                if i == expr_numb:
                    return recurse_group(input_stream, j)
                else:
                    # munching group tokenss
                    same_expr = True
                    inside_groups = 0
                    while same_expr:
                        j += 1
                        if input_stream[j] == "(" and input_stream[j-1] != "\\":
                            inside_groups += 1
                            i += 1
                            if i == expr_numb:
                                return recurse_group(input_stream, j)
                        elif input_stream[j] == ")" and input_stream[j-1] != "\\":
                            if inside_groups != 0:
                                inside_groups -= 1
                            else: 
                                same_expr = False

        elif input_stream[j] == "|":
            i += 1
            j += 1
        else:
            j += 1

def convert(s): 
    # initialization of string to "" 
    new = "" 
  
    # traverse in the string  
    for x in s: 
        new += x  
  
    # return string  
    return new 
      
if __name__ == '__main__':
    experiments = False
    show_a = False
    #if len(sys.argv) > 1:
    #input_stream = FileStream(sys.argv[1])
    #else:
    if len(sys.argv) == 1:
        input_stream = InputStream(sys.stdin.readline().strip())
    else:
        input_stream = InputStream(sys.argv[1].strip())
    if len(sys.argv) > 2 and (sys.argv[2] == "-e"):
        experiments = True
    elif len(sys.argv) > 2 and sys.argv[2] == "-t":
        show_a = True

    lexer = PCRELexer(input_stream)
    token_stream = CommonTokenStream(lexer)
    parser = PCREParser(token_stream)
    tree = parser.parse()

    #lisp_tree_str = tree.toStringTree(recog=parser)
    #print(lisp_tree_str)

    visitor = MyVisitor()
    fsm = visitor.visit(tree)
    if show_a:
        try:
            fsm.show()
        except:
            print("Graph too big to display")
    #if not experiments:
    #    try:
    #        fsm.show()
    #    except:
    #        print("Graph too big to display")
    states = interesting_states(fsm)
    a = minimize_automaton(fsm, states)
    #if not experiments:
    #    a.show()
    #a.show()
    eda = determine_eda(a)
    one = False
    p = None
    EXPR = 0
    for k in eda:
        path1 = eda[k][0]
        path2 = eda[k][1]
        prefix = None
        bad_sub = find_subexpression(str(input_stream), k.expr_numb)
        if "{" in bad_sub and "}" in bad_sub:
            subs = []
            in_stuff = bad_sub[1:-2]
            start_ind = 0
            cont = False

            end_ind = in_stuff.find("|", start_ind)
            if end_ind == -1:
                cont = True
            
            if not cont:
                while end_ind != -1:
                    subs.append(in_stuff[start_ind:end_ind])
                    start_ind = end_ind + 1
                    end_ind = in_stuff.find("|", start_ind)
                subs.append(in_stuff[start_ind:])

                for i in range(len(subs)):
                    can_do = False
                    can_doers = []
                    sub = subs[i]
                    if sub.find("}", 0) < len(sub) - 1 and sub[sub.find("}", 0)+1] != " ":
                        after = sub[sub.find("}", 0)+1:]

                        for j in range(len(subs)):
                            if j != i:
                                if after[0] in subs[j]:
                                    can_do = True
                                    can_doers += (i, j)

                    can_really_do = False
                    pumps = []
                    pump_vals = []
                    if can_do:
                        for c in can_doers:
                            if "{" in subs[i] and "{" in subs[j] and "}" in subs[i] and "}" in subs[j] and not can_really_do:
                                start_ind_1 = 0
                                start_ind_2 = 0
                                out = False


                                while start_ind_1 >= 0 and start_ind_2 >= 0:
                                    if not out:
                                        s_1 = subs[i].find("{", start_ind_1) + 1 
                                        pumps.append(subs[i][start_ind_1: s_1 - 1])
                                        s_2 = subs[j].find("{", start_ind_2) + 1 
                                        val1 = subs[i][subs[i].find("{", start_ind_1) + 1]
                                        val2 = subs[i][subs[i].find("}", start_ind_1) - 1]
                                        if val2 == ",":
                                            try:
                                                val2 = int(val1) + 10
                                            except:
                                                val2 = 0

                                        val3 = subs[j][subs[j].find("{", start_ind_2) + 1]
                                        val4 = subs[j][subs[j].find("}", start_ind_2) - 1]
                                        if val4 == ",":
                                            try:
                                                val4 = val3 + 10
                                            except:
                                                val4 = 0
                                        try:
                                            r1 = range(int(val1), int(val2) + 1)
                                            r2 = range(int(val3), int(val4) + 1)

                                            r1 = set(r1)
                                            if not r1.intersection(r2):
                                                out = True
                                            start_ind_1 = subs[i].find("{", s_1 + 1)                                             
                                            start_ind_2 = subs[j].find("{", s_2 + 1)                                             
                                            pump_vals.append(r1.intersection(r2).pop())
                                        except:
                                            pass

                                if start_ind_1 >= 0 or start_ind_2 >= 0:
                                    out = True
                                    pumps = []
                                    pump_vals = []

                                if not out:
                                    start_ind_1 = 0
                                    start_ind_2 = 0
                                    s_1 = subs[i][start_ind_1:subs[i].find("{", start_ind_1)]
                                    pump = s_1
                                    s_2 = subs[j][start_ind_1:subs[j].find("{", start_ind_2)]
                                    ind_1 = subs[i].find("}", start_ind_1) + 1
                                    ind_2 = subs[j].find("}", start_ind_2) + 1
                                    start_ind_1 = subs[i].find("{", ind_1) + 1
                                    start_ind_2 = subs[j].find("{", ind_2) + 1
                                    pump += pumps[0] * pump_vals[0]
                                    ind = 1
                                    if start_ind_1 == 0 and start_ind_2 == 0:
                                        s_1 += subs[i][ind_1:]
                                        pump += subs[i][ind_1:]
                                        s_2 += subs[j][ind_2:]

                                    while start_ind_1 > 0:
                                        s_1 += subs[i][ind_1:start_ind_1 - 1]
                                        if not one:
                                            pump += subs[i][ind_1:start_ind_1 - 1]
                                        s_2 += subs[j][ind_2:start_ind_2 - 1]
                                        ind_1 = subs[i].find("}", start_ind_1) + 1
                                        ind_2 = subs[j].find("}", start_ind_2) + 1
                                        start_ind_1 = subs[i].find("{", ind_1) + 1
                                        start_ind_2 = subs[j].find("{", ind_2) + 1
                                        if not one:
                                            pump += pumps[ind] * pump_vals[ind]


                                    if s_1 == s_2:
                                        can_really_do = True
                                        if not one:
                                            prefix, _, suffix = exploit_string(fsm, k, path1, path2)
                                            EXPR = k.expr_numb
                                    else:
                                        pumps = []
                                        pump_vals = []

        else:
            can_really_do = True
            EXPR = k.expr_numb


        if can_really_do:
            print("Bad subexpression " + bad_sub)
            if not experiments and not one:
                if prefix is None:
                    prefix, pump, suffix = exploit_string(fsm, k, path1, path2)
                if prefix is None:
                    print("Exploit string could not be found")
            one = True
    done = False
    found = False
    for k in range(1, 20):
        try:
            bad_sub = find_subexpression(str(input_stream), k)
            if bad_sub == None:
                done = True
            if "{" in bad_sub and "}" in bad_sub and EXPR != k and not done:
                subs = []
                in_stuff = bad_sub[1:-2]
                start_ind = 0
                cont = False

                end_ind = in_stuff.find("|", start_ind)
                if end_ind == -1:
                    cont = True
                
                if not cont:
                    while end_ind != -1:
                        subs.append(in_stuff[start_ind:end_ind])
                        start_ind = end_ind + 1
                        end_ind = in_stuff.find("|", start_ind)
                    subs.append(in_stuff[start_ind:])

                    for i in range(len(subs)):
                        can_do = False
                        can_doers = []
                        sub = subs[i]
                        if sub.find("}", 0) < len(sub) - 1 and sub[sub.find("}", 0)+1] != " ":
                            after = sub[sub.find("}", 0)+1:]
                        else:
                            after = ""

                        for j in range(len(subs)):
                            if j == i:
                                can_do = False
                            else:
                                can_do = True
                                can_doers += (i, j)

                        can_really_do = False
                        pumps = []
                        pump_vals = []
                        if can_do:
                            for c in can_doers:
                                if "{" in subs[i] and "{" in subs[j] and "}" in subs[i] and "}" in subs[j] and not can_really_do:
                                    start_ind_1 = 0
                                    start_ind_2 = 0
                                    out = False


                                    while start_ind_1 >= 0 and start_ind_2 >= 0:
                                        if not out:
                                            s_1 = subs[i].find("{", start_ind_1) + 1 
                                            pumps.append(subs[i][start_ind_1: s_1 - 1])
                                            s_2 = subs[j].find("{", start_ind_2) + 1 
                                            val1 = subs[i][subs[i].find("{", start_ind_1) + 1]
                                            val2 = subs[i][subs[i].find("}", start_ind_1) - 1]
                                            if val2 == ",":
                                                try:
                                                    val2 = int(val1) + 10
                                                except:
                                                    val2 = 0

                                            val3 = subs[j][subs[j].find("{", start_ind_2) + 1]
                                            val4 = subs[j][subs[j].find("}", start_ind_2) - 1]
                                            if val4 == ",":
                                                try:
                                                    val4 = val3 + 10
                                                except:
                                                    val4 = 0
                                            try:
                                                r1 = range(int(val1), int(val2) + 1)
                                                r2 = range(int(val3), int(val4) + 1)

                                                r1 = set(r1)
                                                if not r1.intersection(r2):
                                                    out = True
                                                start_ind_1 = subs[i].find("{", s_1 + 1)                                             
                                                start_ind_2 = subs[j].find("{", s_2 + 1)                                             
                                                pump_vals.append(r1.intersection(r2).pop())
                                            except:
                                                pass

                                    if start_ind_1 >= 0 or start_ind_2 >= 0:
                                        out = True
                                        pumps = []
                                        pump_vals = []

                                    if not out:
                                        start_ind_1 = 0
                                        start_ind_2 = 0
                                        s_1 = subs[i][start_ind_1:subs[i].find("{", start_ind_1)]
                                        if not one:
                                            pump = s_1
                                        s_2 = subs[j][start_ind_1:subs[j].find("{", start_ind_2)]
                                        ind_1 = subs[i].find("}", start_ind_1) + 1
                                        ind_2 = subs[j].find("}", start_ind_2) + 1
                                        start_ind_1 = subs[i].find("{", ind_1) + 1
                                        start_ind_2 = subs[j].find("{", ind_2) + 1
                                        if not one:
                                            pump += pumps[0] * pump_vals[0]
                                        ind = 1
                                        if start_ind_1 == 0 and start_ind_2 == 0:
                                            s_1 += subs[i][ind_1:]
                                            if not one:
                                                pump += subs[i][ind_1:]
                                            s_2 += subs[j][ind_2:]

                                        while start_ind_1 > 0:
                                            s_1 += subs[i][ind_1:start_ind_1 - 1]
                                            if not one:
                                                pump += subs[i][ind_1:start_ind_1 - 1]
                                            s_2 += subs[j][ind_2:start_ind_2 - 1]
                                            ind_1 = subs[i].find("}", start_ind_1) + 1
                                            ind_2 = subs[j].find("}", start_ind_2) + 1
                                            start_ind_1 = subs[i].find("{", ind_1) + 1
                                            start_ind_2 = subs[j].find("{", ind_2) + 1
                                            if not one:
                                                pump += pumps[ind] * pump_vals[ind]


                                        if s_1 == s_2:
                                            can_really_do = True
                                            found = True
                                            if not one:
                                                prefix = ""
                                                suffix = "~"
                                            #prefix, _, suffix = exploit_string(fsm, eda, path1, path2)
                                        else:
                                            pumps = []
                                            pump_vals = []


        except:
            done = True
        if found:
            if not experiments and EXPR != i:
                print("Bad subexpression " + bad_sub)
            if not one:
                prefix = ""
                suffix = "~"
                p = pump
                one = True
    if one:
        print("EDA present")
        print("Exploit string")
        if prefix is None:
            prefix = ""
        if prefix != "" and prefix[0] == "^": 
            prefix = prefix[1:]
        print("Prefix: " + prefix)
        if p is not None:
            pump = p
        print("Pump: " + pump)
        if suffix is None:
            suffix = "~"
        if len(suffix) >= 2 and suffix[-2] == "$": 
            suffix = suffix[:-2] + suffix[-1]
        print("Suffix: " + suffix)
    else:
        print("EDA not present")

