# Generated from PCRE.g4 by ANTLR 4.7.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .PCREParser import PCREParser
else:
    from PCREParser import PCREParser

# This class defines a complete generic visitor for a parse tree produced by PCREParser.

class PCREVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by PCREParser#parse.
    def visitParse(self, ctx:PCREParser.ParseContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#alternation.
    def visitAlternation(self, ctx:PCREParser.AlternationContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#expr.
    def visitExpr(self, ctx:PCREParser.ExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#element.
    def visitElement(self, ctx:PCREParser.ElementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#oneGreedy.
    def visitOneGreedy(self, ctx:PCREParser.OneGreedyContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#onePossessive.
    def visitOnePossessive(self, ctx:PCREParser.OnePossessiveContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#oneLazy.
    def visitOneLazy(self, ctx:PCREParser.OneLazyContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#plusGreedy.
    def visitPlusGreedy(self, ctx:PCREParser.PlusGreedyContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#plusPossessive.
    def visitPlusPossessive(self, ctx:PCREParser.PlusPossessiveContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#plusLazy.
    def visitPlusLazy(self, ctx:PCREParser.PlusLazyContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#starGreedy.
    def visitStarGreedy(self, ctx:PCREParser.StarGreedyContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#starPossessive.
    def visitStarPossessive(self, ctx:PCREParser.StarPossessiveContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#starLazy.
    def visitStarLazy(self, ctx:PCREParser.StarLazyContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#nElements.
    def visitNElements(self, ctx:PCREParser.NElementsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#nElementsGreedy.
    def visitNElementsGreedy(self, ctx:PCREParser.NElementsGreedyContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#nElementsPossessive.
    def visitNElementsPossessive(self, ctx:PCREParser.NElementsPossessiveContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#nElementsLazy.
    def visitNElementsLazy(self, ctx:PCREParser.NElementsLazyContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#nmGreedy.
    def visitNmGreedy(self, ctx:PCREParser.NmGreedyContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#nmPossessive.
    def visitNmPossessive(self, ctx:PCREParser.NmPossessiveContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#nmLazy.
    def visitNmLazy(self, ctx:PCREParser.NmLazyContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#charHyphenOpCC.
    def visitCharHyphenOpCC(self, ctx:PCREParser.CharHyphenOpCCContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#charOpCC.
    def visitCharOpCC(self, ctx:PCREParser.CharOpCCContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#opCC.
    def visitOpCC(self, ctx:PCREParser.OpCCContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#charHyphenCC.
    def visitCharHyphenCC(self, ctx:PCREParser.CharHyphenCCContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#charCC.
    def visitCharCC(self, ctx:PCREParser.CharCCContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#normalCC.
    def visitNormalCC(self, ctx:PCREParser.NormalCCContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#backreference.
    def visitBackreference(self, ctx:PCREParser.BackreferenceContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#backreference_or_octal.
    def visitBackreference_or_octal(self, ctx:PCREParser.Backreference_or_octalContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#capture.
    def visitCapture(self, ctx:PCREParser.CaptureContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#non_capture.
    def visitNon_capture(self, ctx:PCREParser.Non_captureContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#comment.
    def visitComment(self, ctx:PCREParser.CommentContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#option.
    def visitOption(self, ctx:PCREParser.OptionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#option_flags.
    def visitOption_flags(self, ctx:PCREParser.Option_flagsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#option_flag.
    def visitOption_flag(self, ctx:PCREParser.Option_flagContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#look_around.
    def visitLook_around(self, ctx:PCREParser.Look_aroundContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#subroutine_reference.
    def visitSubroutine_reference(self, ctx:PCREParser.Subroutine_referenceContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#conditional.
    def visitConditional(self, ctx:PCREParser.ConditionalContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#backtrack_control.
    def visitBacktrack_control(self, ctx:PCREParser.Backtrack_controlContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#newline_convention.
    def visitNewline_convention(self, ctx:PCREParser.Newline_conventionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#callout.
    def visitCallout(self, ctx:PCREParser.CalloutContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#atom.
    def visitAtom(self, ctx:PCREParser.AtomContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#cc_atom.
    def visitCc_atom(self, ctx:PCREParser.Cc_atomContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#shared_atom.
    def visitShared_atom(self, ctx:PCREParser.Shared_atomContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#literal.
    def visitLiteral(self, ctx:PCREParser.LiteralContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#cc_literal.
    def visitCc_literal(self, ctx:PCREParser.Cc_literalContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#shared_literal.
    def visitShared_literal(self, ctx:PCREParser.Shared_literalContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#number.
    def visitNumber(self, ctx:PCREParser.NumberContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#octal_char.
    def visitOctal_char(self, ctx:PCREParser.Octal_charContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#octal_digit.
    def visitOctal_digit(self, ctx:PCREParser.Octal_digitContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#digits.
    def visitDigits(self, ctx:PCREParser.DigitsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#digit.
    def visitDigit(self, ctx:PCREParser.DigitContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#name.
    def visitName(self, ctx:PCREParser.NameContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#alpha_nums.
    def visitAlpha_nums(self, ctx:PCREParser.Alpha_numsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#non_close_parens.
    def visitNon_close_parens(self, ctx:PCREParser.Non_close_parensContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#non_close_paren.
    def visitNon_close_paren(self, ctx:PCREParser.Non_close_parenContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by PCREParser#letter.
    def visitLetter(self, ctx:PCREParser.LetterContext):
        return self.visitChildren(ctx)



del PCREParser