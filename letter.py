
class Letter:

    def __init__(self, expr_numb: int, char: str=None, eps=False, priority=1, character=False, char_range=[]):
        self.__expr_numb = expr_numb
        self.__char = char
        self.__eps = eps
        self.__priority = priority
        self.__character = character
        self.__char_range = char_range

    @property
    def expr_numb(self):
        return self.__expr_numb

    @property
    def char(self):
        return self.__char

    @property
    def eps(self):
        return self.__eps

    @property
    def priority(self):
        return self.__priority

    @priority.setter
    def priority(self, priority):
        self.__priority = priority

    @property
    def character(self):
        return self.__character

    @property
    def char_range(self):
        return self.__char_range

    def __str__(self):
        string = ''
        if self.eps:
            string = 'ε' + str(self.priority)
        elif self.character:
            for l in self.__char_range:
                if l[0] != l[-1] and l[0] != '\\':
                    string += str(l[0]) + '-' + str(l[-1])
                elif l[0] != l[-1]:
                    string += str(l[0]) + str(l[1])
                else:
                    string += str(l[0])
        else:
            string = self.char
        return string

