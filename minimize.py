
from state import State
from edge import Edge
from letter import Letter
from automata import Automata

def interesting_states(fsm: Automata) -> set:
    states = set()
    for state in fsm.states:
        for k, l in state.next_states.items():
            if k != '':
                for v in l:
                    states.add(v[0])
                #states.add(v)

    return states

def minimize_automaton(fsm: Automata, states: set) -> Automata:
    a = Automata()
    new_states = {}

    for state in states:
        new_states[state.name] = State(state.expr_numb, state.name)

    for state in states:
        #marked = {}
        #for k, l in state.next_states.items():
        #    for s, p in l:
        #        marked[k + str(s.name)] = (s, False)
        for goto_state in states:
            #if state == goto_state:
            #    continue
            #pass


            s1 = State(state.expr_numb, state.name)
            if state == goto_state:
                s2 = s1
            else:
                s2 = State(state.expr_numb, goto_state.name)
                
            dfs(state, goto_state, state.next_states, '', a,
                    new_states[state.name], new_states[goto_state.name])

    #a.show()
    return a
    #a.initial_state = fsm.initial_state
    #a.final_state = fsm.final_state

    #for state in states:
    #    pass


#marked should be list of transitions
def dfs(state: State, goto_state: State, marked: dict, string: str, a: Automata,
        statec: State, goto_statec: State):
    for k, l in marked.items():
        i = 0
        for v in l:
            if v[0].name == goto_state.name and string != '':
                a.add_transition(statec, goto_statec, 
                        Letter(1, string, eps=True if string == '' else False, 
                        character=True if len(string) > 1 else False,
                        char_range=string))
                return

            if k == '' and not v[2]:
                marked[k][i] = [v[0], v[1], True]
                dfs(state, goto_state, v[0].next_states, string, a, statec,
                        goto_statec)
                marked[k][i] = [v[0], v[1], False]

            elif k != '' and string == '' and not v[2]:
                if v[0] == goto_state:
                    l = Letter(1, string + k, eps=False, 
                            character=True if len(k) > 1 else False,
                            char_range=string + k)
                    a.add_transition(statec, goto_statec, l)

                marked[k][i] = [v[0], v[1], True]
                dfs(state, goto_state, v[0].next_states, string + k, a, statec,
                        goto_statec)
                marked[k][i] = [v[0], v[1], False]

            else:
                return

            i += 1



