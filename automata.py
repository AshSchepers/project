
from state import State
from edge import Edge
from letter import Letter
import graphviz
from collections import defaultdict
import tempfile

class Automata:

    """Represents an fsm given a regex. """

    def __init__(self):
        self.__states = set()
        self.__accept_states = set()
        self.__initial_state = None
        self.__final_state = None
        self.cur_state = None

    @property
    def initial_state(self) -> State:
        return self.__initial_state

    @initial_state.setter
    def initial_state(self, state: State):
        self.__states.add(state)
        self.__initial_state = state

    @property
    def final_state(self):
        return self.__final_state

    @final_state.setter
    def final_state(self, state: State):
        self.__states.add(state)
        if self.__final_state is not None:
            self.__accept_states.remove(self.__final_state)
        self.__final_state = state
        self.__accept_states.add(state)

    @property
    def accept_states(self) -> set:
        return self.__accept_states

    @property
    def states(self) -> set:
        return self.__states

    @states.setter
    def states(self, s: set()):
        self.__states = s


    def add_accept_state(self, state: State):
        #if state in self._states:
        self.__accept_states.add(state)

    def add_transition(self, state1: State, state2: State, t: Letter):
        #if t.eps:
        #    pass
        #else:
        state1.add_transition(t, state2)
        # TODO don't take into account these states next states
        self.__states.add(state1)
        self.__states.add(state2)

    def create_graphviz_object(self) -> graphviz.Digraph:
        """
        Creates a Graphviz object representing the
        NFA of the current instance.
        """
        digraph = graphviz.Digraph('dfa')
        digraph.graph_attr['rankdir'] = 'LR'

        edges = defaultdict(lambda: defaultdict(list))

        for state in self.__states:
            shape = 'doublecircle' if state in self.accept_states else 'circle'
            digraph.node(name='q{}'.format(state.name), shape=shape, constraint='false')

            for k, to_states in state.next_states.items():
                for to_state in to_states:
                    val = k if k != '' else 'ε' + str(to_state[1])
                    edges[state][to_state[0]].append(val)

        for from_state in edges:
            for to_state, letters in edges[from_state].items():
                #print(letters[0])
                digraph.edge('q{}'.format(from_state.name),
                             'q{}'.format(to_state.name),
                             ','.join(map(str, letters)))

        digraph.node('', shape='plaintext', constraint='true')

        #for start_state in self._start_states:
        if self.__initial_state is not None:
            digraph.edge('', 'q{}'.format(self.__initial_state.name))

        return digraph

    def show(self):
        """
        Graphs the NFA using graphviz, the NFA will
        immediately be shown in a PDF file when this
        method is called.
        """
        self.create_graphviz_object().view(tempfile.mkstemp('gv')[1], cleanup=True)

