 
from state import State

class Edge:

    """
    state_from : state from which edge is pointing
    state_to   : destination point of edge
    trans_type : 0 represents epsilon transition
                 1 represents character transition
    character  : for a character transition, what character to transition on
    priority   : for an epsilon transition, priority of transition
    """
    def __init__(self, state_from: State, state_to: State, trans_type: int,
            character='', priority=0):
        self.__state_from = state_from
        self.__state_to = state_to
        self.__trans_type = trans_type
        self.__character = character
        self.__priority = priority

    @property
    def state_from(self):
        return self.__state_from

    @property
    def state_to(self):
        return self.__state_to

    @property
    def trans_type(self):
        return self.__trans_type

    @property
    def character(self):
        return self.__character

    @property
    def priority(self):
        return self.__priority
